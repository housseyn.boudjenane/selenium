public enum Browser {

    CHROME("webdriver.chrome.driver", "src/resources/drivers/chromedriver.exe"),
    FIREFOX("webdriver.gecko.driver", "src/resources/drivers/geckodriver.exe");
    // Ajoutez d'autres navigateurs au besoin

    private final String propertyKey;
    private final String driverPath;

    Browser(String propertyKey, String driverPath) {
        this.propertyKey = propertyKey;
        this.driverPath = driverPath;
    }

    public String getPropertyKey() {
        return propertyKey;
    }

    public String getDriverPath() {
        return driverPath;
    }
    }