import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ChallengeRPAv2 {

    private WebDriver driver;
    private ArrayList<Map<String,String>> listJdd;
    private Map<String,String> data;
    private String firstName,lastName,companyName,RoleInCompany,address,email,phoneNumber;


    // XPath pour les éléments de la page
    String start= "/html/body/app-root/div[2]/app-rpa1/div/div[1]/div[6]/button";
    String First_Name= "//input[@ng-reflect-name='labelFirstName']";
    String Last_Name= "//input[@ng-reflect-name='labelLastName']";
    String Campany_Name= "//input[@ng-reflect-name='labelCompanyName']";
    String Adresse= "//input[@ng-reflect-name='labelAddress']";
    String labelRole= "//input[@ng-reflect-name='labelRole']";
    String labelEmail= "//input[@ng-reflect-name='labelEmail']";
    String labelPhone= "//input[@ng-reflect-name='labelPhone']";
    String button= "//input[@value='Submit']";

    // Méthode pour ecrire
    public void inpuMethod(String xpath,String var) {
        driver.findElement(By.xpath(xpath)).sendKeys(var);
    }

    // Méthode pour cliquer
    public void clickMethod(String xpath)  {
        driver.findElement(By.xpath(xpath)).click();
    }


    public ArrayList<Map<String, String>> loadCsvJDD(String nomDuFichier) throws IOException {
        String csvFilePath = "src/test/resources/" + nomDuFichier + ".csv";
        ArrayList<Map<String, String>> listJDD = new ArrayList<>();
        List<String[]> list =
                Files.lines(Paths.get(csvFilePath))
                        .map(line -> line.split("\\\\r\\\\n"))
                        .collect(Collectors.toList());
        for (int j = 1; j < list.size(); j++) {
            Map<String, String> jdd = new HashMap<>();
            String[] titres = list.get(0)[0].split(",");
            String[] val = list.get(j)[0].split((","));
            for (int i = 0; i < titres.length; i++) {
                jdd.put(titres[i], val[i]);
            }
            listJDD.add(jdd);
        }
        return listJDD;
    }

    // Cas de test principal
    @Test
    public void executeTest() {
        /* On recup l'utilisateur pour se log */

        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("https://www.rpachallenge.com/");
        driver.manage().window().maximize();
        clickMethod(start);
        try {
            listJdd= loadCsvJDD("english");
            for(int i=0; i<10;i++)
            {
                data = listJdd.get(i);
                firstName = data.get("First Name");
                lastName = data.get("Last Name");
                companyName = data.get("Company Name");
                RoleInCompany = data.get("Role in Company");
                address = data.get("Address");
                email = data.get("Email");
                phoneNumber = data.get("Phone Number");

                inpuMethod(First_Name, firstName);
                inpuMethod(Last_Name, lastName);
                inpuMethod(Campany_Name, companyName);
                inpuMethod(Adresse, address);
                inpuMethod(labelRole, RoleInCompany);
                inpuMethod(labelEmail, email);
                inpuMethod(labelPhone, phoneNumber);
                clickMethod(button);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}