import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.*;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;

public class InternetHovers {

    // Variables
    private WebDriver driver;
    private String URL = "https://the-internet.herokuapp.com/";

    // XPaths
    private String hoverXPath = "//*[@href='/hovers']";
    private String profilXPath = "//*[@id='content']/div/div[1]/img";
    private String userXPath = "//*[@id='content']/div/div[1]/div/h5";

    // Méthode pour clicker sur un élément
    public void clickMethod(String xpath) {
        WebElement cellule = driver.findElement(By.xpath(xpath));
        cellule.click();
    }

    // Méthode pour pointer la souris sur un élément
    public void moveMouse(String xpath) {
        driver.switchTo().defaultContent();
        Actions bouger = new Actions(driver);
        bouger.moveToElement(driver.findElement(By.xpath(xpath))).build().perform();
    }

    // Avant chaque cas de test
    @Before
    public void debut() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Implicit wait: 10 seconds
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        // Navigauer à l'URL
        driver.get(URL);

        // Maximiser la fenêtre
        driver.manage().window().maximize();
    }

    // Cas de test
    @Test
    public void executer () throws InterruptedException {

        // Cliquer sur hovers
        clickMethod(hoverXPath);

        // Bouger la souris sur le profil
        moveMouse(profilXPath);

        // Le profil apparait ?
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(2));
        WebElement userProfil = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(userXPath)));
        assertTrue(userProfil.isDisplayed());
    }

    // Après chaque cas de test
    @After
    public void fin() {
        // """*** Close browser ***"""
        driver.quit();
    }
}