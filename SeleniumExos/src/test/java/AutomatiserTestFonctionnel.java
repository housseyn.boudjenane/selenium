import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.junit.Assert.*;

public class AutomatiserTestFonctionnel {

    private WebDriver driver;
    private String URL = "https://petstore.octoperf.com/actions/Catalog.action";
    private String username = "j2ee";
    private String password = "j2ee";

    // Cette méthode est exécutée avant chaque cas de test
    @Before
    public void Debut() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Naviguer sur l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test principal
    @Test
    public void Executer (){
        // Trouver et cliquer sur le lien "Se connecter"
        WebElement connexion = driver.findElement(By.xpath("//*[@id='MenuContent']/a[2]"));
        connexion.click();

        // Trouver le champ de saisie du nom d'utilisateur, effacer le contenu existant et saisir un nouveau nom d'utilisateur
        WebElement fieldUsername = driver.findElement(By.xpath("//input[@name='username']"));
        fieldUsername.clear();
        fieldUsername.sendKeys(username);

        // Trouver le champ de saisie du mot de passe, effacer le contenu existant et saisir un nouveau mot de passe
        WebElement fieldPassword = driver.findElement(By.xpath("//input[@name='password']"));
        fieldPassword.clear();
        fieldPassword.sendKeys(password);

        // Trouver et cliquer sur le bouton "Connexion"
        WebElement boutonLogin = driver.findElement(By.xpath("//input[@name='signon']"));
        boutonLogin.click();

        // Assertions pour vérifier l'apparition d’un message de bienvenu et du lien "Sign out"
        assertEquals("Welcome ABC!", (driver.findElement(By.xpath("//*[@id='WelcomeContent']")).getText()));
        assertTrue(driver.findElement(By.xpath("//*[@href='/actions/Account.action?signoff=']")).isDisplayed());

        // Naviguer vers la catégorie Fish
        WebElement fish = driver.findElement(By.xpath("//*[@id='SidebarContent']/a[1]/img"));
        fish.click();

        // Assertion pour l'affichage de la liste des produits disponible pour la catégorie Fish
        assertEquals("Fish", (driver.findElement(By.xpath("//*[@id='Catalog']/h2")).getText()));

        // Sélectionner un produit, Ajouter un item au panier "add to cart"
        WebElement Produit = driver.findElement(By.xpath("//*[@href='/actions/Catalog.action?viewProduct=&productId=FI-SW-02']"));
        Produit.click();
        WebElement Panier = driver.findElement(By.xpath("//*[@href='/actions/Cart.action?addItemToCart=&workingItemId=EST-3']"));
        Panier.click();

        // Assertion pour vérifier si la page du panier d'achat est affichée
        assertEquals("Shopping Cart", (driver.findElement(By.xpath("//*[@id='Cart']/h2")).getText()));

        // Passer la quantité commandée à 2 et cliquer sur "update cart"
        WebElement quantite = driver.findElement(By.xpath("//input[@name='EST-3']"));
        quantite.clear();
        quantite.sendKeys("2");
        WebElement cart = driver.findElement(By.xpath("//input[@name='updateCartQuantities']"));
        cart.click();

        // Assertions pour prix total est égal au double du prix à l’unité
        WebElement prixUnitaire = driver.findElement(By.xpath("//*[@id='Cart']/form/table/tbody/tr[2]/td[6]"));
        WebElement prixTotal = driver.findElement(By.xpath("//*[@id='Cart']/form/table/tbody/tr[2]/td[7]"));
        double prixUnit = Double.parseDouble(prixUnitaire.getText().substring(1));
        double prixTot = Double.parseDouble(prixTotal.getText().substring(1));
        assertEquals(2 * prixUnit, prixTot, 0.001);
    }

    // Cette méthode est exécutée après chaque cas de test
    @After
    public void Fin() {
        // Fermer le navigateur
        driver.quit();
    }
}