import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HotelDragDrop {

    private WebDriver driver;
    private String url = "http://localhost/TutorialHtml5HotelPhp/";
    private String name = "resa 1";

    // XPaths
    private final String xpathReservationButton = "//*[@id='dp']/div[3]/div[3]/div/div[2]/div[1]";
    private final String xpathReservationCell = "//div[@class='scheduler_default_event_inner']";
    private final String xpathNewReservationPopup = "//*[@id='f']/h1";
    private final String xpathNameField = "//*[@id='name']";
    private final String xpathSaveButton = "//*[@id='f']/div[9]/input";
    private final String xpathCancelButton = "//a[normalize-space()='Cancel']";
    private final String xpathTomorrowCell = "//*[@id='dp']/div[3]/div[3]/div/div[2]/div[6]";
    private final String xpathUpdateSuccessMessage = "//div[contains(text(),'Update successful')]";

    // Méthode exécutée avant chaque cas de test
    @Before
    public void setup() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Implémenter un Implicit wait de 10 secondes
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // Naviguer sur l'URL spécifiée
        driver.get(url);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test
    @Test
    public void executeTest() {

        // Vérifier si la connexion a réussi
        assertEquals("HTML5 Hotel Room Booking (JavaScript/PHP)", driver.findElement(By.xpath("//*[@id='logo']/a")).getText());

        // Cliquer sur le bouton de réservation
        WebElement reservationButton = driver.findElement(By.xpath(xpathReservationButton));
        reservationButton.click();

        // Affichage de la pop-up d’une nouvelle réservation
        driver.switchTo().frame(0);
        assertEquals("New Reservation", driver.findElement(By.xpath(xpathNewReservationPopup)).getText());

        // Réserver au nom de resa 1
        WebElement nameField = driver.findElement(By.xpath(xpathNameField));
        nameField.clear();
        nameField.sendKeys(name);

        // Cliquer sur save
        WebElement saveButton = driver.findElement(By.xpath(xpathSaveButton));
        saveButton.click();
        driver.switchTo().defaultContent();
        driver.navigate().refresh();

        // Cliquer sur le bouton de réservation
        WebElement reservationCell = driver.findElement(By.xpath(xpathReservationCell));
        reservationCell.click();

        // La réservation apparaît bien sur le planning
        driver.switchTo().frame(0);
        assertTrue(driver.findElement(By.xpath(String.format("//*[@value='%s']", name))).isDisplayed());

        // Quitter la page de réservation
        WebElement cancelButton = driver.findElement(By.xpath(xpathCancelButton));
        cancelButton.click();
        driver.switchTo().defaultContent();

        // Bouger la réservation sur la chambre 1 au lendemain
        Actions actions = new Actions(driver);
        WebElement tomorrowCell = driver.findElement(By.xpath(xpathTomorrowCell));
        actions.clickAndHold(reservationCell).moveToElement(tomorrowCell).release(tomorrowCell).build().perform();

        // Le message update success s’affiche
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(7));
        WebElement updateSuccessMessage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathUpdateSuccessMessage)));
        assertTrue(updateSuccessMessage.isDisplayed());
    }

    // Méthode exécutée après chaque cas de test
    @After
    public void fin() {
        // Fermer le navigateur
        driver.quit();
    }
}