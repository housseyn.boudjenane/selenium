import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class Guru99_FrameSynchronisation {

    WebDriver driver;
    String URL = "https://demo.guru99.com/test/newtours/";
    String name = "mercury";
    String password = "mercury";

    // Variables pour les XPath
    String xpathCookies = "//button[@id='save']";
    String xpathUser = "//input[@name='userName']";
    String xpathPassword = "//input[@name='password']";
    String xpathLogin = "//input[@name='submit']";
    String xpathSucces1 = "//h3[contains(text(),'Login Successfully')]";
    String xpathSucces2 = "//h3[.='Login Successfully']";
    String xpathSucces3 = "//h3[contains(text(),'Login Successfully')]";

    // Méthode exécutée avant chaque cas de test
    @Before
    public void setup() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Implémenter un Implicit wait de 10 secondes
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // Naviguer sur l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test principal
    @Test
    public void executeTest() {

        // Accepter les cookies
        driver.switchTo().frame("gdpr-consent-notice");
        WebElement c = driver.findElement(By.xpath(xpathCookies));
        c.click();

        // Remplir le formulaire : Login
        driver.switchTo().defaultContent();
        WebElement u = driver.findElement(By.xpath(xpathUser));
        u.clear();
        u.sendKeys(name);
        // Remplir le formulaire : Password
        WebElement p = driver.findElement(By.xpath(xpathPassword));
        p.clear();
        p.sendKeys(password);

        // Cliquer sur le bouton de connexion
        WebElement l = driver.findElement(By.xpath(xpathLogin));
        l.click();

        // Assertions pour vérifier si la connexion a réussi
        assertEquals("Login Successfully", driver.findElement(By.xpath(xpathSucces1)).getText());
        assertEquals("Login Successfully", driver.findElement(By.xpath(xpathSucces2)).getText());
        assertTrue(driver.findElement(By.xpath(xpathSucces3)).isDisplayed());
    }

    // Méthode exécutée après chaque cas de test
    @After
    public void fin() {
        // Fermer le navigateur
        driver.quit();
    }
}