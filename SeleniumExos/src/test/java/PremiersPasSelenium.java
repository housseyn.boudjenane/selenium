import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.junit.Assert.assertEquals;

public class PremiersPasSelenium {

    private WebDriver driver;
    private final String URL = "https://latavernedutesteur.fr";

    // Configuration avant chaque test
    @Before
    public void Debut() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Naviguer sur l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Test d'exemple avec assertions
    @Test
    public void Executer() {
        // Récupérer le titre de la page et l'afficher en console
        System.out.println("Le titre récupéré : " + driver.getTitle());

        // Assertion pour vérifier le titre attendu
        assertEquals("Titre Correct", "La taverne du testeur", driver.getTitle());

        // Mettre l’Assertion en échec intentionnellement
        assertEquals("Titre Incorrect", "La taverne du dévelopeur", driver.getTitle());
    }

    // Fermeture du navigateur après chaque test
    @After
    public void Fin() {
        // Fermer le navigateur
        driver.quit();
    }
}