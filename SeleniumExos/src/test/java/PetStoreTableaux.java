import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import static org.junit.Assert.*;

// Classe de test pour l'utilisation de Selenium avec JUnit
public class PetStoreTableaux {

    private WebDriver driver;
    private String url = "https://petstore.octoperf.com/actions/Catalog.action";
    private String username = "j2ee";
    private String password = "j2ee";

    // XPaths
    private String xpathConnexion = "//*[@id='MenuContent']/a[2]";
    private String xpathFieldUsername = "//input[@name='username']";
    private String xpathFieldPassword = "//input[@name='password']";
    private String xpathBoutonLogin = "//input[@name='signon']";
    private String xpathWelcomeMessage = "//*[@id='WelcomeContent']";
    private String xpathSignOutLink = "//*[@href='/actions/Account.action?signoff=']";
    private String xpathDogLink = "//*[@href='/actions/Catalog.action?viewCategory=&categoryId=DOGS']";
    private String xpathDogSection = "//*[@id='Catalog']/h2";
    private String xpathDalmationLink = "//*[@id='Catalog']/table/tbody/tr[%s]/td[1]/a";
    private String xpathDalmationSection = "//*[@id='Catalog']/h2";
    private String xpathDogsLink = "//*[@id='QuickLinks\']/a[2]/img";
    private String xpathBulldogLink = "//*[@id='Catalog']/table/tbody/tr[%s]/td[1]/a";
    private String xpathBulldogSection = "//*[@id='Catalog']/h2";

    // Méthode pour retourner le numéro de ligne d'un élément dans un tableau
    public int retournerNumeroDeLigne(String s){
        int ligneCourante = 1;
        List<WebElement> lignes = driver.findElements(By.xpath("//*[@id='Catalog']/table/tbody/tr"));
        for(WebElement ligne : lignes){
            List<WebElement> cells = ligne.findElements(By.xpath("td"));
            for(WebElement cell: cells){
                if(cell.getText().equals(s)){
                    return ligneCourante;
                }
            }
            ligneCourante++;
        }
        return -1;
    }

    // Méthode exécutée avant chaque cas de test
    @Before
    public void setup() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Naviguer sur l'URL spécifiée
        driver.get(url);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test principal
    @Test
    public void executeTest() {
        // Trouver et cliquer sur le lien "Se connecter"
        WebElement connexion = driver.findElement(By.xpath(xpathConnexion));
        connexion.click();

        // Trouver le champ de saisie du nom d'utilisateur, effacer le contenu existant et saisir un nouveau nom d'utilisateur
        WebElement fieldUsername = driver.findElement(By.xpath(xpathFieldUsername));
        fieldUsername.clear();
        fieldUsername.sendKeys(username);

        // Trouver le champ de saisie du mot de passe, effacer le contenu existant et saisir un nouveau mot de passe
        WebElement fieldPassword = driver.findElement(By.xpath(xpathFieldPassword));
        fieldPassword.clear();
        fieldPassword.sendKeys(password);

        // Trouver et cliquer sur le bouton "Connexion"
        WebElement boutonLogin = driver.findElement(By.xpath(xpathBoutonLogin));
        boutonLogin.click();

        // Assertions pour vérifier l'apparition d’un message de bienvenue et du lien "Sign out"
        assertEquals("Welcome ABC!", driver.findElement(By.xpath(xpathWelcomeMessage)).getText());
        assertTrue(driver.findElement(By.xpath(xpathSignOutLink)).isDisplayed());

        // Naviguer vers la section "Dogs"
        WebElement dog = driver.findElement(By.xpath(xpathDogLink));
        dog.click();

        // Assertion pour vérifier si la section correcte est affichée
        assertEquals("Dogs", driver.findElement(By.xpath(xpathDogSection)).getText());

        // Cliquez sur un chien spécifique (Dalmation)
        WebElement dalmation = driver.findElement(By.xpath(String.format(xpathDalmationLink, retournerNumeroDeLigne("Dalmation"))));
        dalmation.click();

        // Assertion pour vérifier si la section (Dalmation) est affichée
        assertEquals("Dalmation", driver.findElement(By.xpath(xpathDalmationSection)).getText());

        // Revenir à la page des chiens
        WebElement dogsLink = driver.findElement(By.xpath(xpathDogsLink));
        dogsLink.click();

        // Cliquez sur un (Bulldog)
        WebElement bulldog = driver.findElement(By.xpath(String.format(xpathBulldogLink, retournerNumeroDeLigne("Bulldog"))));
        bulldog.click();

        // Assertion pour vérifier si la section (Bulldog) est affichée
        assertEquals("Bulldog", driver.findElement(By.xpath(xpathBulldogSection)).getText());
    }

    // Méthode exécutée après chaque cas de test
    @After
    public void fin() {
        // Fermer le navigateur
        driver.quit();
    }
}