import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;

public class InternetMultipleWindows {

    // Variables
    private WebDriver driver;
    private String URL = "https://the-internet.herokuapp.com/";

    // XPaths
    private String windowXPath = "//a[@href='/windows']";
    private String newWindowXPath = "//*[@href='/windows/new']";
    private String messageXPath = "/html/body/div/h3";

    // Méthode pour clicker sur un élément
    public void clickMethod(String xpath) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(xpath));
        highlightMethod(element);
        element.click();
    }

    // Méthode pour mettre en surbrillance un élément
    public void highlightMethod (WebElement element) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
        Thread.sleep(600);
    }

    // Méthode exécutée avant chaque cas de test
    @Before
    public void setup() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Implémenter un Implicit wait de 10 secondes
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // Naviguer sur l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    @Test
    public void executer () throws InterruptedException {

        // Cliquer sur Multiple Windows
        clickMethod(windowXPath);

        // Cliquer sur Opening a new window
        clickMethod(newWindowXPath);

        // Switcher sur la nouvelle fenetre
        Set<String> allWindows = driver.getWindowHandles();
        driver.switchTo().window((String) allWindows.toArray()[1]);

        // Le message apparait ?
        assertEquals("New Window", (driver.findElement(By.xpath(messageXPath)).getText()));
    }

    @After
    public void fin() {
        // """*** Close browser ***"""
        driver.quit();
    }

    public void switchMethod (){
    }
}