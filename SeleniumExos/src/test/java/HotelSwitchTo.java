import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;


public class HotelSwitchTo {

    private WebDriver driver;
    private String URL = "http://localhost/TutorialHtml5HotelPhp/";
    private String name = "resa 1";

    // Méthode exécutée avant chaque cas de test
    @Before
    public void setup() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Implémenter un Implicit wait de 10 secondes
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // Naviguer sur l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test
    @Test
    public void executeTest() {

        // Vérifier si la connexion a réussi
        assertEquals("HTML5 Hotel Room Booking (JavaScript/PHP)", driver.findElement(By.xpath("//*[@id='logo']/a")).getText());

        // Cliquer sur le bouton de reservation
        WebElement cellule = driver.findElement(By.xpath("//*[@id='dp']/div[3]/div[3]/div/div[2]/div[1]"));
        cellule.click();

        // Affichage de la pop-up d’une nouvelle réservation
        driver.switchTo().frame(0);
        assertEquals("New Reservation", driver.findElement(By.xpath("//*[@id='f']/h1")).getText());


        // Reserver au nom de resa 1
        WebElement login = driver.findElement(By.xpath("//*[@id='name']"));
        login.clear();
        login.sendKeys(name);

        // Cliquer sur save
        WebElement save = driver.findElement(By.xpath("//*[@id='f']/div[9]/input"));
        save.click();
        driver.switchTo().defaultContent();
        driver.navigate().refresh();

        // Cliquer sur le bouton de reservation
        WebElement cellule2 = driver.findElement(By.xpath("//div[@class='scheduler_default_event_inner']"));
        cellule2.click();

        // La réservation apparait bien sur le planning
        driver.switchTo().frame(0);
        assertTrue(driver.findElement(By.xpath("//*[@value='"+name+"']")).isDisplayed());
    }

    // Méthode exécutée après chaque cas de test
    @After
    public void fin() {
        // Fermer le navigateur
        driver.quit();
    }
}