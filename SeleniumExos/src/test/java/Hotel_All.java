import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import java.time.LocalDate;
import static org.junit.Assert.*;

public class Hotel_All {

    // Variables
    private WebDriver driver;
    private String URL = "http://localhost/TutorialHtml5HotelPhp/";
    private String name = "resa 1";

    // XPaths
    private String logoXPath = "//*[@id='logo']/a";
    private String reservationButtonXPath = "//*[@id=\"dp\"]/div[3]/div[3]/div/div[2]/div[2]";
    private String newReservationTitleXPath = "//*[@id='f']/h1";
    private String nameInputXPath = "//*[@id='name']";
    private String startInputXPath = "//input[@id='start']";
    private String endInputXPath = "//input[@id='end']";
    private String saveButtonXPath = "//input[@type='submit']";
    private String reservationCellXPath = "//div[contains(text(), 'resa 1')]";
    private String cancelButtonXPath = "//a[normalize-space()='Cancel']";
    private String dateReservXpath = "//div[@class='scheduler_default_event_inner' and contains(text(), '%s')]";
    private String deleteButtonXPath = "//*[@class='scheduler_default_event_delete']";
    private String deletedMessageXPath = "//div[contains(text(),'Deleted')]";

    @Before
    public void debut() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Implicit wait de 3 seconds
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // Naviguer sur l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    @Test
    public void executer () throws InterruptedException {
        // Verifier que la connexion est réussie
        assertEquals("HTML5 Hotel Room Booking (JavaScript/PHP)", driver.findElement(By.xpath(logoXPath)).getText());

        // Cliquer sur la cellule de rservation
        WebElement cellule = driver.findElement(By.xpath(reservationButtonXPath));
        cellule.click();

        // pop-up pour nouvelle reservation ?
        driver.switchTo().frame(0);
        assertEquals("New Reservation", driver.findElement(By.xpath(newReservationTitleXPath)).getText());

        // Reserver au nom de "resa 1"
        WebElement login = driver.findElement(By.xpath(nameInputXPath));
        login.clear();
        login.sendKeys(name);

        // Reserver à partir d'aujourdhui
        LocalDate dateJour = LocalDate.now();
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        driver.findElement(By.xpath(startInputXPath)).clear();
        driver.findElement(By.xpath(startInputXPath)).sendKeys(dateJour.format(formatDate) + "T12:00:00");

        // Reserver jusqu'à demain
        LocalDate demain = dateJour.plusDays(1);
        driver.findElement(By.xpath(endInputXPath)).clear();
        driver.findElement(By.xpath(endInputXPath)).sendKeys(demain.format(formatDate) + "T12:00:00");

        // Cliquer sur SAVE
        WebElement save = driver.findElement(By.xpath(saveButtonXPath));
        save.click();

        // Cliquer sur la cellule de rservation
        WebElement cellule2 = driver.findElement(By.xpath(reservationCellXPath));
        cellule2.click();

        // La reservation apparait ?
        driver.switchTo().frame(0);
        assertTrue(driver.findElement(By.xpath("//*[@value='" + name + "']")).isDisplayed());

        // Quitter la pop-up de reservation
        WebElement cancel = driver.findElement(By.xpath(cancelButtonXPath));
        cancel.click();
        driver.switchTo().defaultContent();

        // Deplacer la reservation pour le jour d'apres
        LocalDate lendemain = dateJour.plusDays(2);
        cellule2.click();
        driver.switchTo().frame(0);
        driver.findElement(By.xpath(startInputXPath)).clear();
        driver.findElement(By.xpath(startInputXPath)).sendKeys(demain.format(formatDate) + "T12:00:00");
        driver.findElement(By.xpath(endInputXPath)).clear();
        driver.findElement(By.xpath(endInputXPath)).sendKeys(lendemain.format(formatDate) + "T12:00:00");

        // Cliquer sur SAVE
        WebElement save2 = driver.findElement(By.xpath(saveButtonXPath));
        save2.click();

        // Supprimer la reservation
        driver.switchTo().defaultContent();
        Actions bouger = new Actions(driver);

        // Formatez la date selon le modèle HTML
        DateTimeFormatter resDate = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        // Pointer la souri sur la cellule de supression
        Thread.sleep(1000);
        bouger.moveToElement(driver.findElement(By.xpath(String.format(dateReservXpath, resDate)))).build().perform();

        // Pointer la souri sur le bouton de supression
        Thread.sleep(1000);
        bouger.moveToElement(driver.findElement(By.xpath(deleteButtonXPath))).build().perform();
        driver.findElement(By.xpath(deleteButtonXPath)).click();

        // Le message de suppression apparait ?
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(7));
        WebElement successMessage = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(deletedMessageXPath)));
        assertTrue(successMessage.isDisplayed());
    }

    @After
    public void fin() {
        // """*** Close browser ***"""
        driver.quit();
    }
}