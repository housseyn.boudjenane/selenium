import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class SynchronisationSelenium {

    private WebDriver driver;
    String URL = "https://katalon-demo-cura.herokuapp.com/";
    private String username = "John Doe";
    private String password = "ThisIsNotAPassword";

    // XPath pour les éléments de la page
    private String xpathBtnMakeAppointment = "//*[@id='btn-make-appointment']";
    private String xpathTxtUsername = "//*[@id='txt-username']";
    private String xpathTxtPassword = "//*[@id='txt-password']";
    private String xpathBtnLogin = "//*[@id='btn-login']";
    private String xpathMenuLaterale = "//*[@id='menu-toggle']";
    private String xpathLogout = "//*[@href='authenticate.php?logout']";

    // Cette méthode est exécutée avant chaque cas de test
    @Before
    public void Debut() {

        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Implémenter un Implicit wait (attente implicite) de 10 secondes pour tous les éléments du WebDriver
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        // Naviguer vers l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test principal
    @Test
    public void Executer (){
        // Trouver et cliquer sur le lien "Prendre un rendez-vous"
        WebElement connexion = driver.findElement(By.xpath(xpathBtnMakeAppointment));
        connexion.click();

        // Décommenter cette section pour tester un cas d'échec de connexion
//        WebElement connexionFail = driver.findElement(By.xpath("//*[@id='btn-no-make-appointment']"));
//        connexionFail.click();

        // Trouver le champ de saisie du nom d'utilisateur, effacer le contenu existant et saisir un nouveau nom d'utilisateur
        WebElement fieldUsername = driver.findElement(By.xpath(xpathTxtUsername));
        fieldUsername.clear();
        fieldUsername.sendKeys(username);

        // Trouver le champ de saisie du mot de passe, effacer le contenu existant et saisir un nouveau mot de passe
        WebElement fieldPassword = driver.findElement(By.xpath(xpathTxtPassword));
        fieldPassword.clear();
        fieldPassword.sendKeys(password);

        // Trouver et cliquer sur le bouton "Connexion"
        WebElement boutonLogin = driver.findElement(By.xpath(xpathBtnLogin));

        // Instanciation de l’explicit wait
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        // Attendre jusqu'à ce que le bouton "Connexion" soit visible
        wait.until(ExpectedConditions.visibilityOf(boutonLogin));

        // Attendre jusqu'à ce que le bouton "Connexion" soit cliquable
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathBtnLogin)));
        boutonLogin.click();

        // Trouver et cliquer sur le menu latéral
        WebElement menuLateral = driver.findElement(By.xpath(xpathMenuLaterale));
        menuLateral.click();

        // Trouver et cliquer sur le bouton de déconnexion
        WebElement logout = driver.findElement(By.xpath(xpathLogout));
        logout.click();
    }

    // Cette méthode est exécutée après chaque cas de test
    @After
    public void Fin() {
        // Fermer le navigateur
        driver.quit();
    }
}

