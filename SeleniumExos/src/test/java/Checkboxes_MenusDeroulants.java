import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import static org.junit.Assert.*;

public class Checkboxes_MenusDeroulants {

    private WebDriver driver;
    private String URL = "https://petstore.octoperf.com/actions/Catalog.action";
    private String username = "j2ee";
    private String password = "j2ee";

    // Cette méthode est exécutée avant chaque cas de test
    @Before
    public void Debut() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Naviguer sur l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test principal
    @Test
    public void Executer (){
        // Trouver et cliquer sur le lien "Se connecter"
        WebElement connexion = driver.findElement(By.xpath("//*[@id='MenuContent']/a[2]"));
        connexion.click();

        // Trouver le champ de saisie du nom d'utilisateur, effacer le contenu existant et saisir un nouveau nom d'utilisateur
        WebElement fieldUsername = driver.findElement(By.xpath("//input[@name='username']"));
        fieldUsername.clear();
        fieldUsername.sendKeys(username);

        // Trouver le champ de saisie du mot de passe, effacer le contenu existant et saisir un nouveau mot de passe
        WebElement fieldPassword = driver.findElement(By.xpath("//input[@name='password']"));
        fieldPassword.clear();
        fieldPassword.sendKeys(password);

        // Trouver et cliquer sur le bouton "Connexion"
        WebElement boutonLogin = driver.findElement(By.xpath("//input[@name='signon']"));
        boutonLogin.click();

        // Assertions pour vérifier l'apparition d’un message de bienvenu et du lien "Sign out"
        assertEquals("Welcome ABC!", (driver.findElement(By.xpath("//*[@id='WelcomeContent']")).getText()));
        assertTrue(driver.findElement(By.xpath("//*[@href='/actions/Account.action?signoff=']")).isDisplayed());

        // Trouver et cliquer sur le lien "My Account"
        WebElement myAccount = driver.findElement(By.xpath("//*[@href='/actions/Account.action?editAccountForm=']"));
        myAccount.click();

        // Assertions pour vérifier si Affichage de la page de préférence de compte
        assertTrue(driver.findElement(By.xpath("//*[@id=\"Catalog\"]/form/h3[2]")).isDisplayed());

        // Sélectionner "japanese" comme langage de préférence
        WebElement menu = driver.findElement(By.xpath("//*[@name='account.languagePreference']"));
        Select select = new Select(menu);
        select.selectByValue("japanese");

        // Sélectionner "Reptiles" comme animal favori
        WebElement menuR = driver.findElement(By.xpath("//*[@name='account.favouriteCategoryId']"));
        Select select1 = new Select(menuR);
        select1.selectByValue("REPTILES");

        // Enregistrer les informations du compte
        WebElement saveAccount = driver.findElement(By.xpath("//*[@id='Catalog']/form/input"));
        saveAccount.click();

        // Trouver les checkboxes "Enable My list" et "Enable My Banner"
        WebElement ListCheckbox = driver.findElement(By.xpath("//input[@name='account.listOption']"));
        WebElement BannerCheckbox = driver.findElement(By.xpath("//input[@name='account.bannerOption']"));

        // Et vérifier qu'ils sont sélectionnées par défaut
        assertTrue(ListCheckbox.isSelected());
        assertTrue(BannerCheckbox.isSelected());

        //Désélectionner "Enable My list" et Tester
        ListCheckbox.click();
        assertFalse(ListCheckbox.isSelected());
    }

    // Cette méthode est exécutée après chaque cas de test
    @After
    public void Fin() {
        // Fermer le navigateur
        driver.quit();
    }
}