import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

import static org.junit.Assert.*;

public class PetstoreHighlight {

    private WebDriver driver;
    private String URL = "https://petstore.octoperf.com/actions/Catalog.action";
    private String username = "j2ee";
    private String password = "j2ee";

    // XPaths
    private String xpathSignIn = "//*[@id='MenuContent']/a[2]";
    private String xpathUsername = "//input[@name='username']";
    private String xpathPassword = "//input[@name='password']";
    private String xpathLogin = "//input[@name='signon']";
    private String xpathWelcome = "//*[@id='WelcomeContent']";
    private String xpathSignOff = "//*[@href='/actions/Account.action?signoff=']";
    private String xpathDogsCatalog = "//*[@href='/actions/Catalog.action?viewCategory=&categoryId=DOGS']";
    private String xpathDogsHeader = "//*[@id='Catalog']/h2";
    private String xpathDogsID = "//*[@id='Catalog']/table/tbody/tr[%s]/td[1]/a";
    private String xpathDogsLink = "//*[@id='QuickLinks']/a[2]/img";

    // Méthode pour retourner le numéro de ligne d'un élément dans un tableau
    public int numLigne(String s) {
        int ligneCourante = 1;
        List<WebElement> lignes = driver.findElements(By.xpath("//*[@id='Catalog']/table/tbody/tr"));
        for (WebElement ligne : lignes) {
            List<WebElement> cells = ligne.findElements(By.xpath("td"));
            for (WebElement cell : cells) {
                if (cell.getText().equals(s)) {
                    return ligneCourante;
                }
            }
            ligneCourante++;
        }
        return -1;
    }

    // Méthode pour clicker sur un élément
    public void clickMethod(String xpath) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(xpath));
        highlightMethod(element);
        element.click();
    }

    // Méthode pour saisir dans un élément
    public void inputMethod(String xpath, String input) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(xpath));
        highlightMethod(element);
        element.clear();
        element.sendKeys(input);
    }

    // Méthode pour mettre en surbrillance un élément
    public void highlightMethod (WebElement element) throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
        Thread.sleep(600);
    }

    // Avant chaque cas de test
    @Before
    public void debut() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Naviguer sur l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test
    @Test
    public void execution() throws InterruptedException {

        // Cliquer sur "Sign In"
        clickMethod(xpathSignIn);

        // Formulaire de connexion
        // Username
        inputMethod(xpathUsername, username);

        // Password
        inputMethod(xpathPassword, password);

        // Cliquer sur "Login"
        clickMethod(xpathLogin);

        // Tester si la connexion a réussi
        assertEquals("Welcome ABC!", driver.findElement(By.xpath(xpathWelcome)).getText());
        assertTrue(driver.findElement(By.xpath(xpathSignOff)).isDisplayed());

        // Cliquer sur "Dogs"
        clickMethod(xpathDogsCatalog);

        // Tester si la page "Dogs" est affichée
        assertEquals("Dogs", driver.findElement(By.xpath(xpathDogsHeader)).getText());

        // Cliquez sur le ID "Dalmation"
        clickMethod(String.format(xpathDogsID, numLigne("Dalmation")));

        // Tester si la page "Dalmation" est affichée
        assertEquals("Dalmation", driver.findElement(By.xpath(xpathDogsHeader)).getText());

        // Revenir à la page "Dogs"
        clickMethod(xpathDogsLink);

        // Cliquez sur le ID "Bulldog"
        clickMethod(String.format(xpathDogsID, numLigne("Bulldog")));

        // Tester si la page "Bulldog" est affichée
        assertEquals("Bulldog", driver.findElement(By.xpath(xpathDogsHeader)).getText());
    }

    // Après chaque cas de test
    @After
    public void fin() {
        // Fermer le navigateur
        driver.quit();
    }
}