import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ChallengeRPAv1 {

    private WebDriver driver;
    private ArrayList<Map<String, String>> listJdd;
    private Map<String, String> data;

    // XPath pour les éléments de la page
    private static final String START = "/html/body/app-root/div[2]/app-rpa1/div/div[1]/div[6]/button";
    private static final String FIRST_NAME = "//input[@ng-reflect-name='labelFirstName']";
    private static final String LAST_NAME = "//input[@ng-reflect-name='labelLastName']";
    private static final String COMPANY_NAME = "//input[@ng-reflect-name='labelCompanyName']";
    private static final String ADDRESS = "//input[@ng-reflect-name='labelAddress']";
    private static final String ROLE_IN_COMPANY = "//input[@ng-reflect-name='labelRole']";
    private static final String EMAIL = "//input[@ng-reflect-name='labelEmail']";
    private static final String PHONE_NUMBER = "//input[@ng-reflect-name='labelPhone']";
    private static final String BUTTON = "//input[@value='Submit']";

    private static final String PATH_CSV = "src/test/resources/english.csv";

    // Méthode pour écrire
    private void inputMethod(String xpath, String var) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.sendKeys(data.get(var));
    }

    // Méthode pour cliquer
    private void clickMethod(String xpath) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.click();
    }

    private ArrayList<Map<String, String>> loadCsvJDD() {
        try {
            List<String> lines = Files.readAllLines(Paths.get(PATH_CSV));
            String[] headers = lines.get(0).split(",");
            return lines.stream().skip(1).map(line -> {
                String[] values = line.split(",");
                Map<String, String> row = new HashMap<>();
                for (int i = 0; i < headers.length; i++) {
                    row.put(headers[i], values[i]);
                }
                return row;
            }).collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            throw new RuntimeException("Error reading CSV file", e);
        }
    }

    @Test
    public void startChallenge() throws InterruptedException {
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.get("https://www.rpachallenge.com/?lang=EN");
        driver.manage().window().maximize();

        listJdd = loadCsvJDD();
        clickMethod(START);
        for (int i = 0; i <10; i++) {
            data = listJdd.get(i);
            inputMethod(FIRST_NAME, "First Name");
            inputMethod(LAST_NAME, "Last Name");
            inputMethod(COMPANY_NAME, "Company Name");
            inputMethod(ADDRESS, "Address");
            inputMethod(ROLE_IN_COMPANY, "Role in Company");
            inputMethod(EMAIL, "Email");
            inputMethod(PHONE_NUMBER, "Phone Number");
            clickMethod(BUTTON);
        }

        // Ajoutez une pause ou d'autres actions après le clic sur le bouton si nécessaire
        Thread.sleep(2000);

        // Fermez le navigateur après le test
        driver.quit();
    }
}
