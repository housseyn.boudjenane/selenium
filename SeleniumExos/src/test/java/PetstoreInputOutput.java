import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class PetstoreInputOutput {

    // Variables
    private WebDriver driver;
    private String URL = "https://petstore.octoperf.com/actions/Catalog.action";
    String outputFile = "src/resources/output.txt";
    String inputFile =  "src/resources/input.csv";

    // XPath
    private String xpathSignIn = "//*[@id='MenuContent']/a[2]";
    private String xpathUsername = "//input[@name='username']";
    private String xpathPassword = "//input[@name='password']";
    private String xpathLogin = "//input[@name='signon']";
    private String xpathWelcome = "//*[@id='WelcomeContent']";
    private String xpathSignOff = "//*[@href='/actions/Account.action?signoff=']";
    private String xpathDogsCatalog = "//*[@href='/actions/Catalog.action?viewCategory=&categoryId=DOGS']";
    private String xpathDogsHeader = "//*[@id='Catalog']/h2";
    private String xpathDogsID = "//*[@id='Catalog']/table/tbody/tr[%s]/td[1]/a";
    private String xpathDogsLink = "//*[@id='QuickLinks']/a[2]/img";

    // Méthode pour lire un fichier CSV et retourner une liste
    public ArrayList<Map<String, String>> inputCSV(String inputFile) throws IOException {
        // Liste pour stocker les objets Map représentant les lignes du fichier CSV
        ArrayList<Map<String, String>> listJDD = new ArrayList<>();
        // Lire les lignes du fichier CSV et les diviser en tableaux de valeurs
        List<String[]> list = Files.lines(Paths.get(inputFile))
                        .map(line -> line.split("\\\\r\\\\n"))
                        .collect(Collectors.toList());
        // Parcourir les lignes à partir de la deuxième (index 1), car la première ligne contient les titres
        for (int j = 1; j < list.size(); j++) {
            // Créer un objet Map pour stocker les paires clé-valeur de chaque ligne
            Map<String, String> jdd = new HashMap<>();
            // Récupérer les titres de la première ligne
            String[] titres = list.get(0)[0].split(",");
            // Récupérer les valeurs de la ligne actuelle
            String[] val = list.get(j)[0].split(",");
            // Associer chaque titre à sa valeur correspondante et ajouter à l'objet Map
            for (int i = 0; i < titres.length; i++) {
                jdd.put(titres[i], val[i]);
            }
            // Ajouter l'objet Map à la liste
            listJDD.add(jdd);
        }
        // Retourner la liste des objets Map représentant les données du fichier CSV
        return listJDD;
    }

    // Méthode pour écrire dans un fichier texte
    public static void textFileWriting(String pfile, String ptext) throws IOException {
        try {
            // Création d'un objet FileOutputStream pour écrire dans le fichier spécifié
            FileOutputStream outputStream = new FileOutputStream(pfile, true);
            // Création d'un objet OutputStreamWriter pour écrire des caractères en encodage UTF-8
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
            // Création d'un objet BufferedWriter pour écrire des textes dans un tampon
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            // Écriture de la chaîne de caractères suivie d'un saut de ligne dans le fichier
            bufferedWriter.write(ptext);
            bufferedWriter.newLine(); // Ajout d'un saut de ligne
            bufferedWriter.close(); // Fermeture du tampon
        } catch (IOException e) {
            System.out.println("Un problème avec le fichier " + pfile);
            throw e;
        }
    }

    // Méthode pour retourner le numéro de ligne d'un élément dans un tableau
    public int numLigne(String s) {
        int ligneCourante = 1;
        List<WebElement> lignes = driver.findElements(By.xpath("//*[@id='Catalog']/table/tbody/tr"));
        for (WebElement ligne : lignes) {
            List<WebElement> cells = ligne.findElements(By.xpath("td"));
            for (WebElement cell : cells) {
                if (cell.getText().equals(s)) {
                    return ligneCourante;
                }
            }
            ligneCourante++;
        }
        return -1;
    }

    // Méthode pour clicker sur un élément
    public void clickMethod(String xpath) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.click();
    }

    // Méthode pour saisir dans un élément
    public void inputMethod(String xpath, String input) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.clear();
        element.sendKeys(input);
    }


    // Avant chaque cas de test
    @Before
    public void debut() {
        // Définir le chemin du pilote
        System.setProperty("webdriver.chrome.driver", "src/resources/drivers/geckodriver.exe");

        // Créer une instance du navigateur Firefox
        driver = new FirefoxDriver();

        // Naviguer sur l'URL spécifiée
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test
    @Test
    public void execution() throws InterruptedException, IOException {

        // Parcourir le fichier Input
        ArrayList<Map<String, String>> dataList = inputCSV(inputFile);
        Map<String, String> data = dataList.get(0);

        // Recuperer: "username" , "password"
        String login = data.get("login");
        String password = data.get("mdp");
        // Ecrire dans le fichier Output
        textFileWriting(outputFile, login);
        textFileWriting(outputFile, password);

        // Cliquer sur "Sign In"
        clickMethod(xpathSignIn);

        // Formulaire de connexion
        // Username
        inputMethod(xpathUsername, login);

        // Password
        inputMethod(xpathPassword, password);

        // Cliquer sur "Login"
        clickMethod(xpathLogin);

        // Tester si la connexion a réussi
        assertEquals("Welcome ABC!", driver.findElement(By.xpath(xpathWelcome)).getText());
        assertTrue(driver.findElement(By.xpath(xpathSignOff)).isDisplayed());
        // Ecrire dans le fichier Output
        textFileWriting(outputFile, driver.findElement(By.xpath(xpathWelcome)).getText());

        // Cliquer sur "Dogs"
        clickMethod(xpathDogsCatalog);

        // Tester si la page "Dogs" est affichée
        assertEquals("Dogs", driver.findElement(By.xpath(xpathDogsHeader)).getText());
        // Ecrire dans le fichier Output
        textFileWriting(outputFile, driver.findElement(By.xpath(xpathDogsHeader)).getText());

        // Cliquez sur le ID "Dalmation"
        clickMethod(String.format(xpathDogsID, numLigne("Dalmation")));

        // Tester si la page "Dalmation" est affichée
        assertEquals("Dalmation", driver.findElement(By.xpath(xpathDogsHeader)).getText());
        // Ecrire dans le fichier Output
        textFileWriting(outputFile, driver.findElement(By.xpath(xpathDogsHeader)).getText());

        // Revenir à la page "Dogs"
        clickMethod(xpathDogsLink);

        // Cliquez sur le ID "Bulldog"
        clickMethod(String.format(xpathDogsID, numLigne("Bulldog")));

        // Tester si la page "Bulldog" est affichée
        assertEquals("Bulldog", driver.findElement(By.xpath(xpathDogsHeader)).getText());
        // Ecrire dans le fichier Output
        textFileWriting(outputFile, driver.findElement(By.xpath(xpathDogsHeader)).getText());
    }

    // Après chaque cas de test
    @After
    public void fin() {
        // Fermer le navigateur
        driver.quit();
    }
}
